import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";

function mapStateToProps(state) {
  return {notes: state.notes.notes};
}

class NoteCatalog extends Component {


  constructor(props, context) {
    super(props, context);
  }

  render() {
    console.log(this.props.notes);
    return (
      <div>
        <ul>
          {this.props.notes.map(item => {
            let uri = '/note/' + item.id;
            if (item.id == this.props.select) {
              return (<li style={{"background-color" : "#DBE0E6"}}><Link to={uri}>{item.title}</Link></li>)
            } else {
              return (<li><Link to={uri}>{item.title}</Link></li>)
            }

          })}

        </ul>

      </div>
    );
  }
}

export default connect(
  mapStateToProps,
)(NoteCatalog);
