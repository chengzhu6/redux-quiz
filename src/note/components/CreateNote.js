import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";

function mapStateToProps(state) {
  return {state};
}

class CreateNote extends Component {

  constructor(props, context) {
     super(props, context);
     this.newNote = this.newNote.bind(this);
  }

  render() {
    return (
      <div>
        <label htmlFor="title">标题</label>
        <input type="text" id="title"/>
        <label htmlFor="content">正文</label>
        <textarea name="" id="content" cols="30" rows="10"></textarea>
        <Link to="/"><button onClick={this.newNote}>提交</button></Link>
        <Link to="/"><button>取消</button></Link>
      </div>
    );
  }

  newNote() {

  }
}

export default connect(
  mapStateToProps,
)(CreateNote);
