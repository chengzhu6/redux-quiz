const deleteNote = (id) => {
  return {
    type: "DELETE_NOTE",
    detail: id
  }
};

export default deleteNote;
