const getNoteById = (id) => (dispatch) => {
  fetch("http://localhost:8080/api/posts").then((response) => {
    response.json().then((result) => {

      const note = result.find(item =>
        item.id === +id
      );
      dispatch(
        {
          type: "GET_NOTE_BY_ID",
          detail: note
        }
      )
    })
  })
};

export default getNoteById;
