const noteDetail = (state = { detail: {} }, action) => {
  if (action.type === "GET_NOTE_BY_ID") {
    return {
      ...state,
      detail: action.detail,
      isExist: true

    }
  } else if( action.type === "DELETE_NOTE") {
      return {
        ...state,
        isExist : false
      }
  }
  else  {
    return state;
  }
};
export default noteDetail;
