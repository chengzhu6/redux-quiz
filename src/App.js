import React, {Component} from 'react';
import './App.less';
import Home from "./home/components/Home";
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import NoteDetail from "./note/components/NoteDetail";
import getNoteList from "./home/actions/getNoteList";
import {connect} from "react-redux";
import CreateNote from "./note/components/CreateNote";
import Header from "./shared/Header";

class App extends Component{



  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {
    this.props.getNotes();
  }


  render() {
    return (
      <div className="App">
        <Header/>
        <Router>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/note/:id" component={NoteDetail} />
            <Route path="/notes/create" component={CreateNote} />
          </Switch>
        </Router>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getNotes: () => dispatch(getNoteList())
  }
}
export default connect(
  null, mapDispatchToProps)(App);
