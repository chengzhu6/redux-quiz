import React, {Component} from 'react';
import {connect} from 'react-redux';
import './Home.less'

import {Link} from "react-router-dom";

function mapStateToProps(state) {
  return {
    notes: state.notes.notes
  };
}


class Home extends Component {
  constructor(props, context) {
    super(props, context);
  }


  render() {
    return (
      <main>
        <ul className="note-list">
          {this.props.notes.map(item => {
            let uri = '/note/' + item.id;
            return (<li><Link to={uri}>{item.title}</Link></li>)
          })}
          <li><Link to='/notes/create'>新建</Link></li>
        </ul>
      </main>
    );
  }
}

export default connect(
  mapStateToProps)(Home);
