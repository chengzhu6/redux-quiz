const getNoteList = () => (dispatch) => {
  fetch("http://localhost:8080/api/posts").then((response) => {
    response.json().then((result) => {
      dispatch(
        {
          type: "GET_NOTE_LIST",
          notes: result
        }
      )
    })
  })
};

export default getNoteList;
